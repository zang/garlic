% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/select2.R
\name{select2}
\alias{select2}
\title{Select by year(s)}
\usage{
select2(x, years)
}
\arguments{
\item{x}{a data.frame}

\item{years}{a set of years}
}
\value{
a subset-data.frame
}
\description{
Select by year from a \code{data.frame} with years as
\code{\link{rownames}}, and return NA for missing values
}
\details{
Select by year or set/range of years from a \code{data.frame},
that uses years as \code{\link{rownames}}, such as
\code{\link{data.frames}} with tree-ring data used in
\code{\link{dplR}} or \code{\link{bootRes}}.
}

